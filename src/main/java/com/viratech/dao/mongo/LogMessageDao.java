package com.viratech.dao.mongo;

import com.viratech.annotation.MongoDao;
import com.viratech.model.LogMessage;
import org.springframework.stereotype.Repository;

/**
 * Created by Leon on 3/6/2019.
 */


public interface LogMessageDao {

    void createLogMessage(LogMessage logMessage);


}
