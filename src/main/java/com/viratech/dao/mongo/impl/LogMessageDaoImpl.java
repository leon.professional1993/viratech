package com.viratech.dao.mongo.impl;

import com.viratech.annotation.MongoDao;
import com.viratech.dao.mongo.LogMessageDao;
import com.viratech.model.LogMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by Leon on 3/6/2019.
 */

@Repository
@MongoDao
public class LogMessageDaoImpl implements LogMessageDao  {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public void createLogMessage(LogMessage logMessage) {
        mongoTemplate.insert(logMessage);
    }
}
