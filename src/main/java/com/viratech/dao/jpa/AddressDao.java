package com.viratech.dao.jpa;

import com.viratech.annotation.JpaDao;
import com.viratech.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Leon on 3/4/2019.
 */

@Repository
@JpaDao
public interface AddressDao extends CrudRepository<Address,Long> {
}
