package com.viratech.dao.jpa;

import com.viratech.annotation.JpaDao;
import com.viratech.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Leon on 3/3/2019.
 */

@Repository
@JpaDao
public interface PersonDao extends CrudRepository<Person,Long> {

}
