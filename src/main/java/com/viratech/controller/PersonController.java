package com.viratech.controller;

import com.viratech.model.Person;
import com.viratech.service.PersonService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Created by Leon on 3/4/2019.
 */

@Path("/persons")
public class PersonController {

    @Autowired
    PersonService personService;

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response createPerson(Person person){
        personService.createPerson(person);
        return  Response.created(URI.create("/" + person.getId())).build();
    }

    @DELETE
    @Path("{id}")
    public Response deletePerson(@PathParam("id") @NotNull Long id){
        personService.deletePerson(id);
        return Response.ok().build();
    }


    @PUT
    @Consumes("application/json")
    @Path("{id}")
    public Response updatePerson(Person person , @PathParam("id") Long id){
        personService.updatePerson(person,id);
        return Response.ok().build();
    }


    @GET
    @Produces("application/json")
    @Path("/{id}")
    public Person getPerson(@PathParam("id") @NotNull Long id){
            Person person = personService.retrievePerson(id);
            return person;
    }

}
