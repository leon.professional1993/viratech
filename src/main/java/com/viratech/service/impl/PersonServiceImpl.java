package com.viratech.service.impl;

import com.viratech.dao.mongo.LogMessageDao;
import com.viratech.dao.jpa.PersonDao;
import com.viratech.exception.PersonNotFoundException;
import com.viratech.model.LogMessage;
import com.viratech.model.Person;
import com.viratech.service.PersonService;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Leon on 3/3/2019.
 */

@Service
public class PersonServiceImpl implements PersonService {

    Logger logger = Logger.getLogger(PersonService.class);

    @Autowired
    PersonDao personDao;

    @Autowired
    LogMessageDao logMessageDao;

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;


    @Override
    public Person createPerson(Person person) {
        Person result = personDao.save(person);
        //logMessageDao.createLogMessage(new LogMessage("create person" , new Date()));
        logger.info("Person with personId: " + person.getId() + " created");
        return result;
    }

    @Override
    public Person updatePerson(Person person ,Long id) {
        /*Optional<Person> personOptional = personDao.findById(id);
        if (!personOptional.isPresent()){
            throw new PersonNotFoundException();
        }
        person.setId(id);
        Person result = personDao.save(person);
        return result;*/


        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session  = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        if (session.find(Person.class , id) == null){
            throw new PersonNotFoundException();
        }
        person.setId(id);
        Person result = (Person) session.merge(person);
        tx.commit();
        logger.info("Person with personId: " + person.getId() + " updated");
        return result;

        //return null;

    }

    @Override
    public void deletePerson(Long personId) {
        Optional<Person> personOptional = personDao.findById(personId);
        if (!personOptional.isPresent()){
            throw new PersonNotFoundException("Person not found!");
        }
        personDao.delete(personOptional.get());
        logger.info("Person with personId: " + personId + " deleted");

    }

    @Override
    public Person retrievePerson(Long personId) {
        Optional<Person> personOptional = personDao.findById(personId);
        if (!personOptional.isPresent()){
            throw new PersonNotFoundException("Person not found!");
        }
        logger.info("Person with personId: " + personId + " retrieved");
        return personOptional.get();
    }
}
