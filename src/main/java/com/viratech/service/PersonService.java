package com.viratech.service;

import com.viratech.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Leon on 3/3/2019.
 */
public interface PersonService {
    public Person createPerson(Person person);

    public Person updatePerson(Person person , Long id);

    public void deletePerson(Long personId);

    public Person retrievePerson(Long personId);

}
