package com.viratech;

import com.viratech.annotation.JpaDao;
import com.viratech.annotation.MongoDao;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Leon on 3/3/2019.
 */


@SpringBootApplication
@ComponentScan({"com.viratech"})
@EnableJpaRepositories(includeFilters = @ComponentScan.Filter(JpaDao.class))
@EnableMongoRepositories(includeFilters = @ComponentScan.Filter(MongoDao.class))
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class,args);
    }
}
