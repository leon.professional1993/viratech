package com.viratech.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by Leon on 3/5/2019.
 */


@Provider
public class PersonNotFoundException extends ViraTechBusinessException
        implements ExceptionMapper<PersonNotFoundException> {

    public PersonNotFoundException() {
    }

    public PersonNotFoundException(String message) {
        super(message);
    }

    @Override
    public Response toResponse(PersonNotFoundException ex) {
        return Response.status(404).
                entity(ex.getMessage()).
                type("application/json").
                build();
    }
}
