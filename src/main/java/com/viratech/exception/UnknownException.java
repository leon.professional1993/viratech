package com.viratech.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by Leon on 3/5/2019.
 */
public class UnknownException extends Exception implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception ex) {
        return Response.status(500).
                entity(ex.getMessage()).
                type("application/json").
                build();
    }
}
