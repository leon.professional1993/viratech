package com.viratech.exception;

/**
 * Created by Leon on 3/5/2019.
 */
public class ViraTechBusinessException extends RuntimeException {

    public ViraTechBusinessException() {
        super();
    }

    public ViraTechBusinessException(String message) {
        super(message);
    }

    public ViraTechBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public ViraTechBusinessException(Throwable cause) {
        super(cause);
    }

}
