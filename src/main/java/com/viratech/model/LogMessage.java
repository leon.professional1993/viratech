package com.viratech.model;

/*import org.bson.types.ObjectId;*/

import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Leon on 3/6/2019.
 */
public class LogMessage implements Serializable {

    private ObjectId id;
    private String action;
    private Date date;

    public LogMessage(String action, Date date) {
        this.action = action;
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
