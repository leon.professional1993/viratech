package com.viratech.model;

import java.io.Serializable;

/**
 * Created by Leon on 3/3/2019.
 */
public class Address implements Serializable {

    private Long addressId;
    private String path;

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
