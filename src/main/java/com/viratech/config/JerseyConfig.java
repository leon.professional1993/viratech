package com.viratech.config;

import com.viratech.controller.PersonController;
import com.viratech.exception.PersonNotFoundException;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Leon on 3/4/2019.
 */

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(PersonController.class);
        register(PersonNotFoundException.class);
    }


}
